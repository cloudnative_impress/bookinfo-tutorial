# Setting GitLab Repository

## Initialize your GitLab.com repository
Prepare your GitLab.com Account and enable the two-factor authentication & Persornal access token.    

- [Enabling 2FA](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#enabling-2fa)
- [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)

### Create a Project
First you need to copy from original two projects.   
To create a project in GitLab:    

1. In your dashboard, click the green New project button or use the plus icon in the navigation bar. This opens the New project page.
2. On the New project page, choose **Import a project** from diffrent repository.   
3. Inport project under your own projects following [the official documents](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html)

After that, you could register the following repositories.    

:fa-gitlab: Your Application Repository    
https://gitlab.com/${GITLAB_USER}/bookinfo    

:fa-gitlab: Your Manifests Repository    
https://gitlab.com/${GITLAB_USER}/bookinfo-manifests     

## Clone your repository
You can check how to use [the git command](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).
And be ready for your repository information.

```bash
$ export GITLAB_USER= <Your Account Name>
$ export GITLAB_TOKEN= <Your Account Token>
$ export GITLAB_USER_EMAIL= <Your Account email Address>
$ git config --global user.name "${GITLAB_USER}"
$ git config --global user.email "${GITLAB_USER_EMAIL}"
```

Clone the tutorial contents from your GitLab.com repository. 

* Applicaiton Repository
```bash
$ git clone https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/${GITLAB_USER}/bookinfo.git
```

* Manifests Repository
```bash
$ git clone https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/${GITLAB_USER}/bookinfo-manifests.git
```
