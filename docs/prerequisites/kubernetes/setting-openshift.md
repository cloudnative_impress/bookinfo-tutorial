# Setting OpenShift for free

There is a free OpenShift environment. By using this, you can conduct this tutorial.
If you need commands specific to OpenShift, please check tips information in each chapter.
Enjoy and try OpenShift!

## Developer Sandbox for Red Hat OpenShift

Check the following link   
- [Developer Sandbox](https://developers.redhat.com/developer-sandbox)
