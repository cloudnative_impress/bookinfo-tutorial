
# Setting Google Kubernetes Engine
Now, let's actually start up the cluster from the Google Cloud Platform console.    
You can follow the official documentation to prepare a GKE cluster.    
[GKE Quickstart](https://cloud.google.com/kubernetes-engine/docs/quickstart)     

## Preparation to build a GKE cluster (sec:2-2-1)    
Login by command
```
gcloud auth login
```
Here is the output result.
```
Go to the following link in your browser:
  https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=xxxxx
  Enter verification code: <Your Authentication Code>
```
### Create new GCP Project   
Set the envitonments.    
```
## "cloudnative-cicd" is specified in the book.
export PROJECT_ID="<Your Project ID>"
## "Cloud Native CICD" is specified in the book.
export PROJECT_NAME="<Your Project Name>"
gcloud projects create ${PROJECT_ID} \
  --name="${PROJECT_NAME}" --organization=${ORGANIZATION_ID}
```

Check your new GCP Project.   
```
gcloud projects list
PROJECT_ID NAME PROJECT_NUMBER
cloudnative-cicd Cloud Native CICD xxxxxxxxxxx
```

### Enable Billing for your Project  
Enable billing to your account following [the official documents](https://cloud.google.com/billing/docs/how-to/modify-project#confirm_billing_is_enabled_on_a_project)    

!!! Warning
	If your project is not linked to your Cloud Billing account, you will not be able to use the following services.

### Initialize gcloud command for your Project    

After enabling billing, set your default project compute zone    
```bash
gcloud config set project ${PROJECT_ID}
gcloud services enable compute.googleapis.com

export COMPUTE_ZONE=asia-northeast1-a
gcloud config set compute/zone ${COMPUTE_ZONE}
```


### Enable your GKE API    
Enable the Artifact Registry and Google Kubernetes Engine APIs.    

[Enable the APIs](https://console.cloud.google.com/flows/enableapi?apiid=artifactregistry.googleapis.com,container.googleapis.com&_ga=2.176358565.1645979062.1630414603-1533067139.1630400309){ .md-button }

## Build the GKE cluster (sec:2-2-2)    
Check your cluster version. Note that there are always newer versions being updated.
```bash
gcloud container get-server-config \
  --flatten="channels" \
  --filter="channels.channel=REGULAR" \
  --format="yaml(channels.channel,channels.validVersions)"
```
Here is the output result.
```bash
Fetching server config for asia-northeast1-a
---
channels:
  channel: REGULAR
  validVersions:
  - 1.20.8-gke.2100
  - 1.20.8-gke.900
```

Setup the environments.    
```bash
export CLUSTER_NAME=cluster01
export CLUSTER_VERSION='1.20.8-gke.2100'
```

Create GKE Cluster.    
```bash
gcloud container clusters create ${CLUSTER_NAME} \
  --addons HttpLoadBalancing,HorizontalPodAutoscaling,NetworkPolicy \
  --cluster-version ${CLUSTER_VERSION} \
  --release-channel=regular \
  --image-type cos_containerd \
  --enable-autorepair \
  --machine-type=n1-standard-4 \
  --enable-autoscaling --min-nodes 3 --max-nodes 5 --num-nodes 3
```

Check your cluster.   
```bash
gcloud container clusters get-credentials ${CLUSTER_NAME}
kubectl get nodes
```

When you finish create GKE cluster successfully, you can get the following infomations.
```bash
NAME                                      STATUS  ROLES   AGE  VERSION
gke-cluster01-default-pool-47e42fe9-4v69  Ready   <none>  18m  1.20.8-gke.2100
gke-cluster01-default-pool-47e42fe9-fwk2  Ready   <none>  18m  1.20.8-gke.2100
gke-cluster01-default-pool-47e42fe9-ue28  Ready   <none>  18m  1.20.8-gke.2100
```

## Delete your cluster

When you finished your work, don't forget to delete your cluster!!    
```
gcloud container clusters delete ${CLUSTER_NAME}
```

Alternatively, you can set the number of hosts to 0,     
which will greatly reduce the billing while maintaining the configuration.     
```
gcloud container node-pools list --cluster cluster01
gcloud container clusters resize cluster01 --node-pool default-pool --num-nodes 0
```


