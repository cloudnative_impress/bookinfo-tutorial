# Deploy sample BookInfo

Let's deploy the BookInfo application while checking the Kubernetes cluster.   

## Prepare deployment environment (sec:2-4-2)
This sample application will be deployed in **namespace[dev]**.
```bash
$ export NAMESPACE_DEV=dev
$ kubectl create namespace ${NAMESPACE_DEV}
$ kubens ${NAMESPACE_DEV}
```
!!! attention
	If you use Red Hat OpenShift, you must add the privileged SCC(Security Context Constraint) to ServiceAccount.    
        `$ oc adm policy add-scc-to-user anyuid -z bookinfo-reviews`

## BookInfo deployment with 1st version of Reviews

You can deploy BookInfo from a sample repository. First, deploy version 1 of Reviews.
```bash
$ cd ~/bookinfo-manifests/sample/
$ kubectl apply -f ./bookinfo/bookinfo-ratings.yaml
$ kubectl apply -f ./bookinfo/bookinfo-details.yaml
$ kubectl apply -f ./bookinfo/bookinfo-productpage.yaml
$ kubectl apply -l version!=v2,version!=v3 -f ./bookinfo/bookinfo-reviews.yaml
```

After deploying BookInfo Deployment, you can get these results.
```bash
$ kubectl get pods
NAME                          READY  STATUS
details-79c697d759-clz6x      1/1    Running
productpage-65576bb7bf-7hr6p  1/1    Running
ratings-7d99676f7f-mh782      1/1    Running
reviews-v1-545db77b95-sjbdn   1/1    Running
```
### Check BookInfo from cluster inside    
You can access the Service[productpage] by a curl container.   

```bash
$ export CURL_IMAGE="governmentpaas/curl-ssl"
$ kubectl run -it curl --image=${CURL_IMAGE} \
  --rm --restart=Never \
  --command -- curl -s productpage:9080/productpage
```
If you get plane contents, you are accessing the BookInfo correctly.   

### Check BookInfo from outside (sec:2-4-3)    
Also you check BookInfo by GKE Ingress.
When you use GKE ingress, you must select `type:NodePort` in the target Service.    

```bash
$ kubectl patch service productpage -p '{"spec": {"type": "NodePort"}}'
```

After changing to NodePort, check Service Type.    
```bash
$ kubectl get service productpage
NAME        TYPE      CLUSTER-IP    EXTERNAL-IP  PORT(S)
productpage NodePort  10.3.255.125  <none>       9080:32530/TCP
```

Configure Ingress.   
```bash
## Current Directory ~/bookinfo-manifests/sample
$ kubectl apply -f ./bookinfo/bookinfo-ingress.yaml
```

Check the `EXTERNAL ADDRESS` for Ingress[bookinfo].    
```bash
$ kubectl get ingress bookinfo
NAME      CLASS   HOSTS                     ADDRESS          PORTS
bookinfo  <none>  dev-bookinfo.example.com  XXX.XXX.XXX.XXX  80
```
Assign the BookInfo hostname for Linux hosts.    
```bash
$ export BOOKINFO_DOMAIN='dev-bookinfo.example.com'
$ export BOOKINFO_ENDPOINT=$(kubectl get ingress bookinfo -o jsonpath='{..ip}')
$ echo "${BOOKINFO_ENDPOINT} ${BOOKINFO_DOMAIN}" |sudo tee -a /etc/hosts
```
You can retrieve plain text via Ingress.   
```bash
$ curl -s http://dev-bookinfo.example.com/productpage | grep -o "<title>.*</title>"
<title>Simple Bookstore App</title>
```

!!! Tips
	`EXTERNAL ADDRESS` will be configured in hosts file on your Laptop.    
	See [Modify your hosts file](https://docs.rackspace.com/support/how-to/modify-your-hosts-file/)    

**Development BookInfo Pages**   
After set the `EXTERNAL ADDRESS` to your PC's hosts file, you can access BookInfo page from a browser.    
```bash
http://dev-bookinfo.example.com/productpage
```
![BookInfo-Dev-Reviews-v1](./assets/bookinfo-dev-reviews-v1.png)   

## BookInfo deployment with 2nd version of Reviews 

Replace Reviews from v1 to v2.
```bash
## Current Directory: bookinfo-manifests/sample
$ kubectl delete deployment reviews-v1
$ kubectl apply -l version=v2 \
  -f ./bookinfo/bookinfo-reviews.yaml
```
Check Reviews Pod is changed.
```bash
$ kubectl get pods
NAME                          READY  STATUS
details-79c697d759-h5p4q      1/1    Running
productpage-65576bb7bf-8c7pg  1/1    Running
ratings-7d99676f7f-b872q      1/1    Running
reviews-v2-7bf8c9648f-m5zbv   1/1    Running
```

