---
# Sample Tasks    

Get started the basic of `Tasks`.   
This exercise should be done in `dev`namespace.   

```bash
## You don't need to operate these commands if you've already done.
$ export NAMESPACE_DEV=dev
$ kubectl create namespace ${NAMESPACE_DEV}
$ kubens ${NAMESPACE_DEV}
```

## Operation check (sec:3-2-3)
Register the sample two `Tasks` in Tekton pipelies.   

- `Task[git-clone]`: Task to git clone and store contents in volume.
- `Task[source-lister]`: Task to list contents in volume.

![Sample Task](./assets/sample-task.png)    

### Create PVC[bookinfo]
Prepare the volume to store git contents.   
```bash
## Current Directory: bookinfo-manifests/sample/tekton
$ cd ~/bookinfo-manifests/sample/tekton
$ kubectl apply -f ./volumesources/bookinfo-pvc.yaml
```

Check the assignment of volumes.   
```bash
$ kubectl get pv
NAME          CAPACITY  ACCESS MODES ...  STATUS CLAIM STORAGECLASS
pvc-2d636d8b  1Gi       RWO          ...  Bound dev/bookinfo standard
$ kubectl get pvc
NAME      STATUS  VOLUME        CAPACITY  ACCESS MODES  STORAGECLASS
bookinfo  Bound   pvc-2d636d8b  1Gi       RWO           standard
```
### Execute Task[git-clone]   
Register the Task[git-clone].    
```bash
## Current Directory: bookinfo-manifests/sample/tekton
$ kubectl create -f ./tasks/git-clone.yaml
```

Check the `Task` registration.   
```bash
$ tkn task list
NAME       DESCRIPTION
git-clone  These Tasks are Git...

$ tkn task describe git-clone
Name: git-clone
Namespace: dev
```

Register the `TaskRun[git-clone-run]` for execution.   
```bash
$ kubectl create -f ./taskruns/git-clone-run.yaml
```

Confirm the execution of the process by TaskRun.    
```bash
$ tkn taskrun list
NAME                 STARTED         DURATION    STATUS
git-clone-run-4fqwz  53 seconds ago  16 seconds  Succeeded

## Specify the registered TaskRun name.
$ tkn taskrun logs git-clone-run-4fqwz
[clone] + CHECKOUT_DIR=/workspace/output/
[clone] + echo -n 508cf89db535e36c2a36598535aae08850cfbf0f
[clone] + echo -n https://gitlab.com/cloudnative_impress/bookinfo.git
...
```

### Execute Task[source-lister]   
Register the `Task[source-lister]`.     
```bash
## Current Directory: bookinfo-manifests/sample/tekton
$ kubectl apply -f ./tasks/source-lister.yaml
```

Confirm the registered `Tasks`    
```bash
$ tkn task list
NAME           DESCRIPTION
git-clone      These Tasks are Git...
source-lister
```

Execute TaskRun[source-lister-run] without manifest registration.    
```bash
$ tkn task start source-lister --showlog --workspace name=output,claimName=bookinfo
```
Starting tasks, some git contents in the volume are displayed.
```bash
? Value for param ‘contextDir‘ of type ‘string‘? (Default is ‘reviews‘) **reviews**
TaskRun started: source-lister-run-tq6jn
Waiting for logs to be available...
[ls-sources] total 16
[ls-sources] -rw-r--r-- 1 root root 85 ... settings.gradle
[ls-sources] drwxr-xr-x 5 root root 4096 ... reviews-wlpcfg
[ls-sources] drwxr-xr-x 3 root root 4096 ... reviews-application
[ls-sources] -rw-r--r-- 1 root root 101 ... build.gradle
```
