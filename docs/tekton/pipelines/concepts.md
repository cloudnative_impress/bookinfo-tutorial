---
# Core Concepts

!!! quote
        This document a quote from the official documentation.   
	[Tekton Official Document](https://tekton.dev/docs/concepts/)   


## Custom Resources   
Tekton Pipelines consists of `Steps`, `Tasks`, and `Pipelines`.The official documentation explains this area in detail.   

### Steps   
A step is an operation in a CI/CD workflow, such as running some unit tests for a Python web app, or the compilation of a Java program. Tekton performs each step with a container image you provide. For example, you may use the official Go image to compile a Go program in the same manner as you would on your local workstation (go build).   

### Tasks   
A task is a collection of steps in order. Tekton runs a task in the form of a Kubernetes pod, where each step becomes a running container in the pod. This design allows you to set up a shared environment for a number of related steps; for example, you may mount a Kubernetes volume in a task, which will be accessible inside each step of the task.   

### Pipelines   
A pipeline is a collection of tasks in order. Tekton collects all the tasks, connects them in a directed acyclic graph (DAG), and executes the graph in sequence. In other words, it creates a number of Kubernetes pods and ensures that each pod completes running successfully as desired. Tekton grants developers full control of the process: one may set up a fan-in/fan-out scenario of task completion, ask Tekton to retry automatically should a flaky test exists, or specify a condition that a task must meet before proceeding.   

Tasks and pipelines are specified as custom resources in a Kubernetes cluster.   

![Concept Tasks Pipelines](https://tekton.dev/docs/concepts/concept-tasks-pipelines.png)   

## References   
Additionally you can refer the following links.      

- [Introduction to Tekton architecture and design](https://developer.ibm.com/articles/introduction-to-tekton-architecture-and-design/)   
- [Getting started with Tekton and Pipelines](https://developers.redhat.com/blog/2021/01/13/getting-started-with-tekton-and-pipelines)    
