---
# Exercise for Tasks    

Here introduces basic of `Tasks`. This exercise should be done in `dev`namespace.   

```bash
## You don't need to operate these commands if you've already done.
$ export NAMESPACE_DEV=dev
$ kubectl create namespace ${NAMESPACE_DEV}
$ kubens ${NAMESPACE_DEV}
```

!!! Note
	A Task is a collection of Steps that you define and arrange in a specific order of execution as part of your continuous integration flow. A Task is available within a specific namespace, while a ClusterTask is available across the entire cluster.    
        See also: [Tekton Pipelines Tasks](https://tekton.dev/docs/pipelines/tasks/)

## Steps field (sec:3-3-1)
The Steps field is an important field that defines the execution process of the `Task`.
The order in which the Steps appear in this list is the order in which they will execute.    
Refer to the following `Tasks`.

- [Task[git-clone]](https://gitlab.com/cloudnative_impress/bookinfo-manifests/-/blob/main/sample/tekton/tasks/gradle-build.yaml#L12)   
- [Task[soruce-lister]](https://gitlab.com/cloudnative_impress/bookinfo-manifests/-/blob/main/sample/tekton/tasks/source-lister.yaml#L12)

### Register Task      

```bash
## Current Directory: bookinfo-manifests/sample/tekton
$ kubectl create -f ./taskruns/git-clone-run.yaml
$ kubectl apply -f ./tasks/gradle-build.yaml
$ tkn task describe gradle-build
Name: gradle-build
Namespace: dev
…
$ tkn task start gradle-build \
  --showlog \
  --workspace name=output,claimName=bookinfo \
  --param='contextDir=reviews/reviews-application'

TaskRun started: gradle-build-run-zdkcv
…
[gradle-build] BUILD SUCCESSFUL in 12s
[gradle-build] 4 actionable tasks: 4 executed
```

### Check the result of TaskRun   
Check a result from completed task.   

```bash
$ tkn taskrun list gradle-build
NAME                    STARTED        DURATION    STATUS
gradle-build-run-zdkcv  3 minutes ago  59 seconds  Succeeded
```

## Workspaces field (sec:3-3-2)
The Workspaces field (Workspaces) provides the shared file system 
required when the `Task` is instantiated by `TaskRun`.
Workspaces allow you to specify one or more volumes 
that your Task requires during execution.   

Prepare the `VolumeSource` before registration of `Tasks`.   

```bash
## Current Directory: bookinfo-manifests/sample/tekton
$ kubectl apply -f ./volumesources/workspaces-cm.yaml
$ kubectl get configmap workspaces-configmap
NAME                  DATA
workspaces-configmap  1
## You don't need to execute this command when you've already set up.
$ kubectl apply -f ./volumesources/bookinfo-pvc.yaml
$ kubectl get pvc
NAME      STATUS  VOLUME        CAPACITY  ACCESS MODES  STORAGECLASS
bookinfo  Bound   pvc-2d636d8b  1Gi       RWO           standard
```
### Register Task      
Refer to the following Tasks and register it.   

- [Task[show-workspaces]](https://gitlab.com/cloudnative_impress/bookinfo-manifests/-/blob/main/sample/tekton/tasks/show-workspaces.yaml#L6)

```bash
$ kubectl apply -f ./tasks/show-workspaces.yaml
$ kubectl create -f ./taskruns/show-workspaces-run.yaml
```

### Check the result of TaskRun   
Check the Workspaces result from TaskRun. You can find the contents in emptyDir, ConfigMap.   

```bash
$ tkn taskrun list
NAME                       STARTED         DURATION    STATUS
show-workspaces-run-4qpml  32 seconds ago  31 seconds  Succeeded

$ tkn taskrun logs show-workspaces-run-4qpml
[ls-workspaces] /workspace:
[ls-workspaces] total 8
[ls-workspaces] drwxr-xr-x 5 root root 4096 ... workspace-pvc2
[ls-workspaces] drwxrwxrwx 2 root root 6 ... workspace-emptyDir
[ls-workspaces] drwxrwxrwx 3 root root 88 ... workspace-configmap
[ls-workspaces] drwxr-xr-x 3 root root 4096 ... workspace-pvc1
…
[write-to-emptydir] + echo ’Sample Workspaces - VolumeSource emptyDir’
[read-emptydir] Sample Workspaces - VolumeSource emptyDir
[show-configmap] Sample Workspaces - VolumeSource ConfigMap
```

When you delete `TaskRun[show-workspaces-run]` with volumeClaimTemplate,
the volume will be deleted at the same time.    

```
$ tkn taskrun delete show-workspaces-run-4qpml
Are you sure you want to delete TaskRun(s) "show-workspaces-run-4qpml" (y/n): y
TaskRuns deleted: "show-workspaces-run-4qpml"

$ kubectl get pvc
NAME      STATUS  VOLUME        CAPACITY  ACCESS MODES  STORAGECLASS
bookinfo  Bound   pvc-2d636d8b  1Gi       RWO           standard
```

## Parameters field (sec:3-3-3)   
The Parameters field (Params) treats the values in the process.   
You can specify params, such as compilation flags or artifact names, 
that you want to supply to the `Task` at execution time. Params are passed to the `Task` from its corresponding `TaskRun`.

Let's take a look at the Params configuration.   

- [Task[curl]](https://gitlab.com/cloudnative_impress/bookinfo-manifests/-/blob/main/sample/tekton/tasks/curl.yaml#L14)

### Register Task      

Register `Task[curl]` and check its status.
In TaskRun[curl-response-run], we will use Params[url] and Params[options] 
to get the response header of the site.   

```bash
## Current Directory: bookinfo-manifests/sample/tekton
$ kubectl apply -f ./tasks/curl.yaml
$ kubectl create -f ./taskruns/curl-response-run.yaml
```

### Check the result of TaskRun   
You can get the response header by executing the `curl` command.   

```bash
$ tkn taskrun list
NAME                    STARTED         DURATION   STATUS
curl-response-runjmxjl  11 seconds ago  6 seconds  Succeeded

$ tkn taskrun logs -L
[curl] HTTP/1.1 200 OK
[curl] Content-Type: text/html; charset=ISO-8859-1
…
```

## Results field (sec:3-3-4)
A `Task` is able to emit string results that can be viewed 
by users and passed to other `Tasks` in a `Pipeline`.
These results have a wide variety of potential uses.    

The contents of Results will be saved as a file under /tekton/results when `TaskRun` is executed.    
The file is created dynamically by setting Results in the `Task`, 
but the contents of Results need to be defined in the `Task`.   

- [Task[git-clone]](https://gitlab.com/cloudnative_impress/bookinfo-manifests/-/blob/main/sample/tekton/tasks/git-clone.yaml#L75)

## Sidecar field (sec:3-3-5)
The sidecars field specifies a list of Containers to run alongside the Steps in the `Task`.   
Sidecar containers will before Steps are executed, and will be removed after the all Steps in `Task`.   

- [Task[curl-sidecar]](https://gitlab.com/cloudnative_impress/bookinfo-manifests/-/blob/main/sample/tekton/tasks/curl-sidecar.yaml#L37)

### Register Task      
Let's run `TaskRun[curl-sidecar-run]`.

```bash
## Current Directory: bookinfo-manifests/sample/tekton
$ kubectl apply -f ./tasks/curl-sidecar.yaml
$ kubectl create -f ./taskruns/curl-sidecar-run.yaml
```

### Check the result of TaskRun   
When the TaskRun is successful, 
you will see the result of the API connection to the Reviews service.    
At this point, there is no Rating service, 
so you will see that no information about the Rating service has been retrieved.    

```bash
$ tkn taskrun list
NAME                   STARTED        DURATION    STATUS
curl-sidecar-runfhdng  7 minutes ago  17 seconds  Succeeded

$ tkn taskrun logs -L
…
[curl] {"id": "0","reviews": [{ "reviewer": "Reviewer1", "text": "An extremely e
ntertaining play by Shakespeare. The slapstick humour is refreshing!", "rating": {
"error": "Ratings service is currently unavailable"}},{ "reviewer": "Reviewer2",
"text": "Absolutely fun and entertaining. The play lacks thematic depth when comp
ared to other plays by Shakespeare.", "rating": {"error": "Ratings service is curr
ently unavailable"}}]}
```

