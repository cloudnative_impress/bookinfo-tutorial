---
# Installations 

## Install Tekton Pipelines (sec:3-2-2)      
Install Tekton Pipelines on your cluster.    

```bash
$ export TEKTON_PIPELINE_VERSION=v0.27.3
$ kubectl apply -f https://storage.googleapis.com/tekton-releases/pipeline/previous/${TEKTON_PIPELINE_VERSION}/release.yaml
```

!!! attention
        If you use Red Hat OpenShift, you must add the privileged SCC(Security Context Constraint) to ServiceAccount before the install operation.    
	`$ oc new-project tekton-pipelines`    
	`$ oc adm policy add-scc-to-user anyuid -z tekton-pipelines-controller`    
	`$ oc adm policy add-scc-to-user anyuid -z tekton-pipelines-webhook`    
	When you use Tekton Operator, skip these setting and see the operator installation.
	See [Install OpenShift Pipelines](https://github.com/openshift/pipelines-tutorial/blob/master/install-operator.md)

That's completely all. Check the Tekton Pipelines Controllers and Custom Resouces are fine.These objects are installed in `tekton-pipelines` namespace.   

```bash
$ kubectl -n tekton-pipelines get pods
NAME                                          READY  STATUS
tekton-pipelines-controller-6dc5865c88-pmmmp  1/1    Running
tekton-pipelines-webhook-5dd664cb65-c8cqh     1/1    Running

$ kubectl -n tekton-pipelines get configmaps
NAME                    DATA
config-artifact-bucket  0
config-artifact-pvc     0
config-defaults         1
config-leader-election  3
…

$ kubectl api-resources --api-group='tekton.dev'
NAME                SHORTNAMES   APIVERSION            NAMESPACED   KIND
clustertasks                     tekton.dev/v1beta1    false        ClusterTask
conditions                       tekton.dev/v1alpha1   true         Condition
pipelineresources                tekton.dev/v1alpha1   true         PipelineResource
pipelineruns        pr,prs       tekton.dev/v1beta1    true         PipelineRun
pipelines                        tekton.dev/v1beta1    true         Pipeline
runs                             tekton.dev/v1alpha1   true         Run
taskruns            tr,trs       tekton.dev/v1beta1    true         TaskRun
tasks                            tekton.dev/v1beta1    true         Task
```

## Install Tekton Pipelines CLI
Setup Tekton original command(`tkn`) to controll custom objects.

```bash
$ export TEKTON_CLI_VERSION=v0.20.0
$ curl -LO \
https://github.com/tektoncd/cli/releases/download/${TEKTON_CLI_VERSION}/tkn_${TEKTON_CLI_VERSION#v}_Linux_x86_64.tar.gz
$ sudo tar xvzf tkn_${TEKTON_CLI_VERSION#v}_Linux_x86_64.tar.gz -C /usr/local/bin/ tkn
```

Check the install version by `tkn` command.
```bash
$ tkn version
Client version: 0.20.0
Pipeline version: v0.27.3
$ rm -vr tkn_${TEKTON_CLI_VERSION#v}_Linux_x86_64.tar.gz
```

