---
# Pipelines Overview

BookInfo CI/CD Tutorial builds this pipeline by Tekton Pipelines.

![Reviews-deploy-pipelines](assets/reviews-deploy-pipelines.png)

1. [**Get the source code**](git-clone.md): Get the source code from the BookInfo and BookInfo Manifests repositories.   
2. **Build the application**: Use Gradle to build the Reviews service.    
3. **Test the manifest**: Check the manifest policy.
4. **Container Build**: Build the container for the Reviews service from the Dockerfile.
5. **Container Image Scan**: Detects vulnerabilities in the container image.
6. **Container Deploy**: Deploy the container image of the Reviews service.
