
# Installations 

## Install Tekton Triggers (sec:5-2-2)      
Install Tekton Triggers on your cluster.    

```bash
$ export TEKTON_TRIGGER_VERSION=v0.15.0
$ kubectl apply -f \
https://storage.googleapis.com/tekton-releases/triggers/previous/\
${TEKTON_TRIGGER_VERSION}/release.yaml
```

!!! attention
        If you use Red Hat OpenShift, you must add the privileged SCC(Security Context Constraint) to ServiceAccount before the install operation.    
	`$ oc new-project tekton-pipelines`    
	`$ oc adm policy add-scc-to-user anyuid -z tekton-pipelines-controller`    
	`$ oc adm policy add-scc-to-user anyuid -z tekton-pipelines-webhook`    
	When you use Tekton Operator, skip these setting and see the operator installation.
	See [Install OpenShift Pipelines](https://github.com/openshift/pipelines-tutorial/blob/master/install-operator.md)

Tekton Triggers Controllers and Custom Resouces are installed in `tekton-pipelines` namespace.
Interceptors needed to be installed separately as following.

```bash
$ kubectl apply -f \
https://storage.googleapis.com/tekton-releases/triggers/previous/\
${TEKTON_TRIGGER_VERSION}/interceptors.notags.yaml
deployment.apps/tekton-triggers-core-interceptors created
service/tekton-triggers-core-interceptors created
clusterinterceptor.triggers.tekton.dev/cel created
clusterinterceptor.triggers.tekton.dev/bitbucket created
clusterinterceptor.triggers.tekton.dev/github created
clusterinterceptor.triggers.tekton.dev/gitlab created
``` 


```bash
$ kubectl get pods -n tekton-pipelines
NAME READY STATUS
tekton-pipelines-controller-54d84cc998-w2fh2 1/1 Running
tekton-pipelines-webhook-6c75f6bf9b-rnw9s 1/1 Running
tekton-triggers-controller-c558bd9dd-zd7jc 1/1 Running
tekton-triggers-core-interceptors-76c4bc6dd-cbj5v 1/1 Running
tekton-triggers-webhook-55c7cc8c5f-tj69j 1/1 Running

$ kubectl api-resources --api-group=’triggers.tekton.dev’
NAME                     SHORTNAMES   APIVERSION            NAMESPACED   KIND
clusterinterceptors      ci           ...                   false        ClusterInterceptor
clustertriggerbindings   ctb          ...                   false        ClusterTriggerBinding
eventlisteners           el           ...                   true         EventListener
triggerbindings          tb           ...                   true         TriggerBinding
triggers                 tri          ...                   true         Trigger
triggertemplates         tt           ...                   true         TriggerTemplate
```

## Install NGINX Ingress Controller
EventListner creates only Service and Deployment objects. NGINX Ingress Controller allows you to create Ingress objects to expose these items and to receive webhooks from Git repository.

```bash
$ export NGINX_INGRESS_CONTROLLER_VERSION=v0.48.1
$ kubectl apply \
-f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-\
${NGINX_INGRESS_CONTROLLER_VERSION}/deploy/static/provider/cloud/deploy.yaml

$ kubectl get pods -n ingress-nginx
NAME                                        READY STATUS    ...
ingress-nginx-admission-create-z8gxd        0/1   Completed ...
ingress-nginx-admission-patch-bz7qh         0/1   Completed ...
ingress-nginx-controller-7fc74cf778-vj966   1/1   Running   ...

$ kubectl get services -n ingress-nginx
NAME                                 TYPE         ...   PORT(S)
ingress-nginx-controller             LoadBalancer ...   80:30368/TCP,443:31339/TCP
ingress-nginx-controller-admission   ClusterIP    ...   443/TCP
```
Create IngressClass object to specify using NGINX Ingress Controller.
```bash
## Current Directory: bookinfo-manifests/tekton
$ kubectl apply -f ./eventlisteners/nginx-ingress/nginx-ingressclass.yaml
$ kubectl describe ingressclass/nginx
Name: nginx
Labels: app.kubernetes.io/name=nginx
Annotations: ingressclass.kubernetes.io/is-default-class: true
ingressclass.kubernetes.io/ssl-redirect: false
Controller: k8s.io/ingress-nginx
Events: <none>
```

