# Core Concepts

!!! quote
        This document quotes from the official documentation.   
	[Tekton Official Document](https://tekton.dev/docs/triggers/)   


## Custom Resources   
Tekton Triggers mainly consist of `TriggerTemplate`, `TriggerBinding`, and `EventListner`.The official documentation explains this area in detail.   

### TriggerTemplate   
A TriggerTemplate is a resource that specifies a blueprint for the resource, such as a TaskRun or PipelineRun, that you want to instantiate and/or execute when your EventListener detects an event. It exposes parameters that you can use anywhere within your resource’s template.   

### TriggerBinding   
A TriggerBinding is a resource that specifies the fields in the event payload from which you want to extract data as well as the fields in your corresponding TriggerTemplate to populate with the extracted values. In other words, it binds payload fields to fields in the TriggerTemplate and for this to work the fields specified in the TriggerBinding and the corresponding TriggerTemplate must match. You can then use the populated fields in your TriggerTemplate to populate fields in the TaskRun or PipelineRun associated with that TriggerTemplate. Tekton also supports a cluster-scoped version called a ClusterTriggerBinding to encourage reusability across your entire cluster.   

### EventListener   
An EventListener is a Kubernetes object that listens for events at a specified port on your Kubernetes cluster. It exposes an addressable sink that receives incoming event and specifies one or more Triggers. The sink is a Kubernetes service running the sink logic inside a dedicated Pod.
Each Trigger, in turn, allows you to specify one or more TriggerBindings that allow you to extract fields and their values from event payloads, and one or more TriggerTemplates that receive field values from the corresponding TriggerBindings and allow Tekton Triggers to instantiate resources such as TaskRuns and PipelineRuns with that data.   
  

## References   
Additionally you can refer the following links.      

- [Setting Up Tekton Triggers](https://tekton.dev/docs/triggers/install/)   
- [Getting Started with Tekton Triggers](https://github.com/tektoncd/triggers/blob/main/docs/getting-started/README.md)
- [Tekton Triggers code examples](https://github.com/tektoncd/triggers/tree/main/examples)
- [Troubleshooting Tekton Triggers](https://tekton.dev/docs/triggers/troubleshooting/)
