# BookInfo Tutorial Pipelines

Let's take a look at the deployment environment and application lifecycle of this tutorial.   

Tekton Pipelines start building and testing containers 
when a merge request (MR) or commit to GitLab.
We treat the container image as a completed artifact 
after the dynamic tests have finished as expected.
On the other hand, Argo CD automates the deployment process 
after the container and manifest have been fully tested.   
   
In this tutorial, it will introduce two different methods:   

- to deploy completely dynamically
- to manage the deployment process by updating the manifest.

## BookInfo CI/CD Flow
The flow of Tekton Pipelines and Argo CD is in the following order.   

![BookInfo Tutorial Flow](assets/bookinfo-tutorial-flow.png)

1. Push the application source code to the Git repository
2. Start a Tekton Pipelines triggered by the commit operation
3. Retrieve the application source code, build and test it
4. Build and store the container image
5. Test the application deployment using the new container image
6. Update manifest and submit MR
7. Argo CD receives the changes in the manifest repository and deploys to each deployment environment





