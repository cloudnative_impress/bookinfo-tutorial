# BookInfo Repository

## Separating Source Code & Manifests Repositories
This tutorial is based on GitOps best practices to 
maintain separate repositories for application source code and manifests.     

!!! Note
	See also: [Separating Config Vs. Source Code Repositories](https://argoproj.github.io/argo-cd/user-guide/best_practices/#best-practices)

## Deployment Environment for this tutorial (sec:2-1-4)
This tutorial simulates the following four deployment environments using namespaces on a Kubernetes cluster.    

- Development Environment - `dev`namespace (`namespace[dev]` in book)   
- Testing Environment - `test`namespace    
- Staging Environment - `stg`namespace    
- Production Environment - `prod`namespace   

!!! Warning
	In practice, it is recommended to separate clusters for each deployment environment to minimize security and service impact caused by cluster upgrades.   

## Branch Strategy for this tutorial (sec:2-3-4)
In this tutorial, we will apply a separate branching strategy for each repository.    

Application source code repository: [GitHub Flow](https://docs.gitlab.com/ce/topics/gitlab_flow.html#github-flow-as-a-simpler-alternative)   
![GitHub Flow](assets/github-flow.png)

Manifest repository: [GitLab Flow](https://docs.gitlab.com/ce/topics/gitlab_flow.html#environment-branches-with-gitlab-flow)    
![GitLab Flow](assets/gitlab-flow.png)    

