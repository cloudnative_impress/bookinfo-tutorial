# Welcome to BookInfo CI/CD Tutorial

![Bookinfo CI/CD Tutorial](assets/tutorial_logo.png)

You can learn the Kubernetes application lifecycle for containers through this tutorial.   
The content is excellent for those who use [Tekton](https://tekton.dev/) and [Argo](https://argoproj.github.io/) to perform continuous integration and delivery.   

## Target persons
This guide focuses on the Kubernetes application lifecycle, and it's expected that you're familiar with container technology and Kubernetes.   

## Repositories
This tutorial consists of the following two Git repositories.   

:fa-gitlab: The Application Repository (BookInfo)    
[https://gitlab.com/cloudnative_impress/bookinfo](https://gitlab.com/cloudnative_impress/bookinfo)   
:fa-gitlab: The Manifests Repository (BookInfo Manifests)    
[https://gitlab.com/cloudnative_impress/bookinfo-manifests](https://gitlab.com/cloudnative_impress/bookinfo-manifests)    

## Used Products in this tutorial
This tutorial was developed and tested with

```
- Kubernetes: v1.20.8
- Tekton Pipelines : v0.27.3
- Tekton Triggers : v0.15.0
- Nginx Ingress : v0.48.1
- Argo CD : v2.0.5
- Argo Rollouts : v1.0.6
```

## Reference Book
This tutorial is also covered in the following book.   

[:fa-book: Kubernetes CI/CD Pipelines (Amazon)](https://www.amazon.co.jp/dp/4295012750){ .md-button }

<img src="assets/kubecicd_h1.png" alt="Kubernetes CI/CD Pipelines" width="324" height="414">

```yaml
Publisher: Impress Corporation
Release Date: October 19th, 2021
Author: Shingo Kitayama (Red Hat K.K.)
Language: Japanese
Softcover: 400 pages
ISBN-10: 4295012750
ISBN-13: 978-4295012757
```

### Table of Contents

**Chapter 1 - Operational Changes in the Development Process**   
Introduction to the development process that will change from Kubernetes   

**Chapter 2 - Preparing Environment for Cloud Native Development**   
Preparing the environment to implement continuous application lifecycle with Kubernetes   

**Chapter 3 - Overview of Tekton Pipelines**   
Tekton Pipelines architecture and Task details   

**Chapter 4 - Continuous Integration Pipelines**   
Implementing Continuous Integration Pipelines by Tekton Pipelines   

**Chapter 5 - Event-driven pipeline execution**   
Implementing a trigger based pipeline by Tekton Triggers    

**Chapter 6 - Argo CD Overview**    
Learn about the Argo CD architecture and GitOps   

**Chapter 7 - Continuous Delivery Deployment**    
Implementing GitOps with Kustomize and Argo CD   

**Chapter 8 - Continuous Delivery Releasement**    
The overview of Progressive Delivery by Argo Rollouts   
