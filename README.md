---
# Bookinfo Tutorial Site

![Bookinfo Tutorial Logo](https://gitlab.com/cloudnative_impress/bookinfo-tutorial/-/raw/main/docs/assets/tutorial_logo.png)   

You can learn the Kubernetes application lifecycle for containers through this tutorial.   
The content is excellent for those who use [Tekton](https://tekton.dev/) and [Argo](https://argoproj.github.io/) to perform continuous integration and delivery.   

This repository makes up this site.   
See [BookInfo CI/CD Tutorial](https://cloudnative_impress.gitlab.io/bookinfo-tutorial/)   

## Maintenance Policy   
This site is maintained by contributors. We are always looking for people to maintain it with us.   

If you find any bugs or fixes in docs, please send us a Merge Request.    

And you can also use it commercially.       
Please contact the following if you need help.       

- [@spchildren](https://twitter.com/spchildren)


